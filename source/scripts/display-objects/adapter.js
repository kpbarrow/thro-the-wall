function bounds(width, height){
    return {
        width: width,
        height: height
    };
}

function position(x, y){
    return {
        x: x,
        y: y
    };
}

function adapt(parent){
    parent.setSize = function(newBounds){
        this.width = newBounds.width || this.width;
        this.height = newBounds.height || this.height;
    };

    parent.setPosition = function(newPosition){
        this.position.x = newPosition.x || this.position.x;
        this.position.y = newPosition.y || this.position.y;
    };

    parent.getSize = function(){
        return bounds(this.width, this.height);
    };

    parent.getPosition = function(){
        return this.position;
    };

    //TODO: within bounds.
}