var SPEED = 1; //TODO: add getter

function Bat(){
    PIXI.Sprite.call(this, DISPLAY_OBJECTS.BAT.TEXTURE);
    adapt(this);

    this.setSize(bounds(DISPLAY_OBJECTS.BAT.WIDTH, DISPLAY_OBJECTS.BAT.HEIGHT));
    this.setPosition(position((DISPLAY_OBJECTS.STAGE.WIDTH - this.width)/2, DISPLAY_OBJECTS.STAGE.HEIGHT - this.height));

    pressed = {};

    window.addEventListener('keydown', function(event){
        console.log('Key');
       pressed[event.keyCode] = true;
    });

    window.addEventListener('keyup', function(event){
        delete pressed[event.keyCode];
    });

    isPressed = function(keyCode) {
        console.log(pressed);
        console.log(!!pressed[keyCode]);
        return !!pressed[keyCode];
    };
}

Bat.prototype = Object.create(PIXI.Sprite.prototype);

Bat.prototype.moveLeft = function(){
    console.log('Move Left');
    var newPostion = this.position.x - SPEED;
    this.position.x = newPostion <= 0 ? 0 : newPostion;
};

Bat.prototype.moveRight = function(){
    console.log('Move Right');
    var newPostion = this.position.x + SPEED,
        maxPostition = DISPLAY_OBJECTS.STAGE.WIDTH - this.width;
    this.position.x = newPostion  >= maxPostition ? maxPostition : newPostion;
};

Bat.prototype.update = function(){
    var LEFT =79,
        RIGHT = 80;

    if(isPressed(LEFT)){
        this.moveLeft();
    }
    if(isPressed(RIGHT)){
        this.moveRight();
    }
};

